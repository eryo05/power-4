class Puissance4
{
  //plateau bleu
  constructor() {
    this.dom_plateau = [];
    // un entier: 1 ou 2 (le numéro du prochain joueur)
    this.turn = 1;
    /* un entier:
      0: la partie continue
      -1: la partie est nulle
      1: joueur 1 a gagné
      2: joueur 2 a gagné
      */
    this.game_status = 0;
    // Nombre de coups joués
    this.coups = 0;
    // Nombre de lignes
    this.nbLignes = 6;
    // Nombre de colonnes
    this.nbColonnes = 7;

    this.varTimer = 0;
    this.resetTimer = 0;
   

    //plateau bleu
    this.jeu = document.querySelector('#jeu');

    this.t = document.createElement('table');
    this.t.id = 'plateau';

    for (var i = this.nbLignes - 1; i >= 0; i--) {
      var tr = document.createElement('tr');
      this.dom_plateau[i] = [];
      for (var j = 0; j < this.nbColonnes; j++) {
        var td = document.createElement('td');
        td.dataset.column = j;
        tr.appendChild(td);
        this.dom_plateau[i][j] = td;
      }
      this.t.appendChild(tr);
    }
    this.jeu.innerHTML = '';
    this.jeu.appendChild(this.t);

  }

   
   
  // Fonction auxiliaire d'affichage 
  set(row, column, player) {
    // On colore la case
   this.dom_plateau[row][column].className = 'joueur' + player;
    // On décompte le coup
    this.coups++;
    // On passe le tour : 3 - 2 = 1, 3 - 1 = 2
    this.turn = 3 - this.turn;

    if (this.turn == 1){
      this.playerChoice = document.querySelector('#display2');
      this.playerChoice.innerHTML = 'Au joueur 1 <br> Nombre de coup : ' + this.coups;
    }else{
      this.playerChoice = document.querySelector('#display2');
      this.playerChoice.innerHTML = 'Au joueur 2 <br> Nombre de coup : ' + this.coups;
    }

    

    }

  /* Cette fonction ajoute un pion dans une colonne */
  play(column) {
    // Vérifier si la partie est encore en cours
    if (this.game_status != 0) {
      if (window.confirm("La partie est finie!\n\nSouhaitez-vous recommencer?")) { 
        this.resetTimer = 1;
        this.reset();
        }
        
        return;
    }

    // Trouver la première case libre dans la colonne en partant du bas
    var row;
    for (var i = 0; i < this.nbLignes; i++) {
    if (!this.dom_plateau[i][column].className) {
      row = i;
      break;
    }
    }
    if (row === undefined) {
    window.alert("La colonne est pleine!");
    return;
    }

    // Jouer le coup
    this.set(row, column, this.turn);

    // Vérifier s'il y a un gagnant, ou si la partie est finie
    if (this.win(row, column, 'joueur' + (3 - this.turn))) {
    this.game_status = 3 - this.turn;
    } else if (this.coups >= this.nbLignes * this.nbColonnes) {
    this.game_status = -1;
    }

    //Afficher un message si la partie est finie
    switch (this.game_status) {
    case -1: 
      var counter = document.getElementById('timer').innerHTML;
      window.alert("Partie Nulle!! en : " + counter); 
      
      clearInterval(this.refreshIntervalId);
      break;
    case 1:
      var counter = document.getElementById('timer').innerHTML;
      window.alert("Victoire du joueur 1 en : " + counter);
      clearInterval(this.refreshIntervalId);
      break;
    case 2:
      var counter = document.getElementById('timer').innerHTML;
      window.alert("Victoire du joueur 2 en : " + counter);
      clearInterval(this.refreshIntervalId);
      break;
    }
  }

  

  /* Cette fonction vérifie si le coup dans la case `row`, `column` par le joueur `playerName` est un coup gagnant.
  Renvoie :
	true  : si la partie est gagnée par le joueur `playerName`
	false : si la partie continue
  */
  win(row, column, playerName) {
    // Horizontal
  var count = 0;
  for (var j = 0; j < this.nbColonnes; j++) {
  count = (this.dom_plateau[row][j].className == playerName) ? count+1 : 0;
  if (count >= 4) return true;
  }
    // Vertical
  count = 0;
  for (var i = 0; i < this.nbLignes; i++) {
  count = (this.dom_plateau[i][column].className == playerName) ? count+1 : 0;
    if (count >= 4) return true;
  }
    // Diagonal
  count = 0;
  var shift = row - column;
  for (var i = Math.max(shift, 0); i < Math.min(this.nbLignes, this.nbColonnes + shift); i++) {
  count = (this.dom_plateau[i][i - shift].className == playerName) ? count+1 : 0;
    if (count >= 4) return true;
  }
    // Anti-diagonal
  count = 0;
  shift = row + column;
  for (var i = Math.max(shift - this.nbColonnes + 1, 0); i < Math.min(this.nbLignes, shift + 1); i++) {
  count = (this.dom_plateau[i][shift - i].className == playerName) ? count+1 : 0;
  if (count >= 4) return true;
  }

  return false;
  }

  // Cette fonction vide le plateau et remet tout à zéro
  reset() {
  for (var i = 0; i < this.nbLignes; i++) {
  for (var j = 0; j < this.nbColonnes; j++) {
    this.dom_plateau[i][j].className = "";
  }
  }
   
    this.coups = 0;
    this.game_status = 0;

    this.t.addEventListener('click', e => {
      let column = e.target.dataset.column;
      if (column) {
        if (this.resetTimer == 1){
          this.timer(0,0);
          this.resetTimer = 0;
        }

      }

      
       
     });

    
  
    //window.location.reload();
  }


  start () {
    this.t.addEventListener('click', e => {
     let column = e.target.dataset.column;
     if (column) 
      this.play(parseInt(column));
      
      if (this.varTimer == 0){
        this.timer(0,0);
        this.varTimer = 1;
      }
      
    });
  }
  timer(timerMinutes, timerSeconds) {
    this.refreshIntervalId = setInterval(e => {
      timerSeconds++;
      if(timerSeconds >= 60) {
        timerMinutes++;
        timerSeconds = 0; 
      }

      if(timerMinutes.toString().length == 1) {
          timerMinutes = "0" + timerMinutes;
          parseInt(timerMinutes);
        }
      if(timerSeconds.toString().length == 1) {
        timerSeconds = "0" + timerSeconds;
        parseInt(timerSeconds);
        }
      console.log(timerMinutes, timerSeconds);
      let timer = document.querySelector('#timer');
      timer.innerHTML = timerMinutes + ":" + timerSeconds;
    },1000) 
  }
}
// On initialise le plateau et on l'ajoute à l'arbre du DOM (dans la balise d'identifiant `jeu`).
//puissance4.init(document.querySelector('#jeu'));
game = new Puissance4;
game.start();
